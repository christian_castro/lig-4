// =========================== VARIÁVEIS GLOBAIS =========================

const cssRoot = document.querySelector(':root');
const playerONEPickColor = document.querySelector('#player1-color');
const playerTWOPickColor = document.querySelector('#player2-color');
const infoPlayerONE = document.querySelector('#info-player1');
const infoPlayerTWO = document.querySelector('#info-player2');
const main = document.querySelector('main');
const btnPlayAgain = document.querySelector('#btn-again');
const divMain = document.getElementById('Tabuleiro');
const btChangeName = document.getElementById('player-name-bt');
const btnReset = document.getElementById('btnReset');
const playerUm = document.getElementById('score1')
const playerDois = document.getElementById('score2')
const Player1Name = document.getElementById('player1-name')
const Player2Name = document.getElementById('player2-name')
const popUp = document.getElementById('popUp');

let soma = 0;
let somaDois = 0;
//============= Array de mapeamento do tabuleiro ================
let mapping = [];

//============= Informações jogadores ============
const player1 = {
    id: 'player1',
    name: 'Player 1',
    avatar: "styles/imgs/nicholas-bg.png",
    color: '#0095F6',
    avatarWin: "styles/imgs/nich-bg.png",
    score: playerUm
};

const player2 = {
    id: 'player2',
    name: 'Player 2',
    avatar: 'styles/imgs/hudson-bg-4.png',
    color: '#ff0000',
    avatarWin: "styles/imgs/elmago.png",
    score: playerDois
};

let jogadorDaVez = player1;
// ================ CRIAÇÃO DO TABULEIRO =================//
const createBoard = () => {

    for (let coluna = 0; coluna <= 6; coluna++) {

        mapping.push([]);

        let col = document.createElement('div');
        col.classList.add('col')
        col.setAttribute('id', coluna)
        divMain.appendChild(col);

        for (let linha = 0; linha < 6; linha++) {

            mapping[coluna].push(' ');

            let line = document.createElement('div');
            line.setAttribute('id', coluna + '-' + linha)
            line.classList.add('celula');

            col.appendChild(line);
        }
    }
}
createBoard();

// ================= FUNCIONALIDADES DO JOGO ======================//
//  Função principal
const play = (e) => {
    // Verifica em qual coluna foi dado o clique:
    const column = checkColumn(e);
    if (column !== undefined) {
        // Acha a última célula disponível nessa coluna:
        const cellID = getLastCell(column);

        if (cellID !== undefined) {
            // Separa as posições da célula
            const cellPosition = cellID.split('-');

            createPlayerDisk(cellPosition[0], cellPosition[1]);
            checkWinHorizontal(cellPosition[1]);
            checkWinVertical(cellPosition[0]);
            checkWinDiagonal();
            if(checkDraw()) showDraw();
            switchPlayer();
        }
    }
}

function checkColumn(e) {
    let col = e.target
    if (e.target.id !== 'Tabuleiro') {
        while (!(col.classList.contains('col'))) {
            col = col.parentNode;
        }
        return col;
    }
}

function getLastCell(col) {
    const colID = col.id;
    const line = mapping[colID].indexOf(' ');
    if (line !== -1) {
        return colID + '-' + line;
    }
    return undefined;
}

const createPlayerDisk = (coluna, linha) => {
    let celula = document.getElementById(coluna + '-' + linha);
    const disk = document.createElement('img');
    disk.classList.add(jogadorDaVez.id);
    disk.src = jogadorDaVez.avatar;
    mapping[coluna][linha] = jogadorDaVez.id;
    celula.appendChild(disk);
}

const switchPlayer = () => {
    if (jogadorDaVez === player1) {
        jogadorDaVez = player2;
        infoPlayerTWO.classList.toggle('itsturn');
        infoPlayerONE.classList.toggle('itsturn');
    } else {
        jogadorDaVez = player1;
        infoPlayerONE.classList.toggle('itsturn');
        infoPlayerTWO.classList.toggle('itsturn');
    }
}

const showVictory = () => {
    let display = jogadorDaVez;
    const popUp = document.getElementById('popUp');
    popUp.classList.remove('hidden');
    popUp.innerHTML = `<p>${display.name} WON!</p>`;
    popUp.innerHTML += `<img src="${display.avatarWin}" alt='myimage'>`
}

function showDraw () {
    const popUp = document.getElementById('popUp');
    popUp.classList.remove('hidden');
    popUp.innerHTML = `<p>Empatou</p>`;
    popUp.innerHTML += `<img src="${"https://pbs.twimg.com/media/E3hipkvVcAULJLf?format=png&name=small"}" alt='myimage'>`
}
function addScore () {
    jogadorDaVez.score.innerText++;
}
 //==========================Condições de vitória=======================//   

    //================Verifica Horizontal=============//
    function checkWinHorizontal(cellLine) {
        const player1win = 'player1player1player1player1';
        const player2win = 'player2player2player2player2';

        let lineElements = '';
        mapping.forEach(column => lineElements += column[cellLine]);

        if (lineElements.includes(player1win)) {
            addScore(); 
            showVictory();
        }
        if (lineElements.includes(player2win)) {
            addScore();  
            showVictory();    
        } 
    }
    
    //================Verifica Vertical==============//
    function checkWinVertical(col) {
        const player1win = 'player1player1player1player1';
        const player2win = 'player2player2player2player2';

        let verticalElements = '';
        verticalElements = mapping[col].join('')

        if (verticalElements.includes(player1win)) {
            addScore(); 
            showVictory();
        }

        if (verticalElements.includes(player2win)) {
            addScore();  
            showVictory();
        }
    }
    
    //========Verifica Diagonais==========//
    function checkWinDiagonal() {
        //diagonal crescente
        for (let k = 0; k < 3; k++) {

            for (let i = 0; i < 4; i++) {
                let arrToVerify = [];

                for (let j = 0; j < 4; j++) {
                    arrToVerify.push(mapping[i + j][j + k])
                }
                
                if (arrToVerify.join('').includes('player1player1player1player1')) {
                    addScore(); 
                    showVictory();
                }

                if (arrToVerify.join('').includes('player2player2player2player2')) {
                    addScore(); 
                    showVictory();
                }
            }
        }
        //Diagonal Decrescente
        for (let k = 5; k > 2; k--) {

            for (let i = 0; i < 4; i++) {
                let arrToVerify = [];

                for (let j = 0; j < 4; j++) {
                    arrToVerify.push(mapping[i + j][k - j]);
                }

                if (arrToVerify.join('').includes('player1player1player1player1')) {
                    addScore(); 
                    showVictory();
                }

                if (arrToVerify.join('').includes('player2player2player2player2')) {
                    addScore();     
                    showVictory();
                }
            }
        }
    }
//==============Empate==========//
    function checkDraw() {
        let emptyCols = 0;
        for(let i = 0; i < mapping.length ;i++){
            const col = mapping[i].join('');
            if(!col.includes(' ')) emptyCols++;
        }
        if(emptyCols === mapping.length) return true;
        return false;
    }


// Rotina para mudança de nome dos jogadores
//  Pega o container do input e mostra ele. Espera o click do botao confirma
function changeName1() {
    // Nome player1:
    const inputName1container = document.getElementById('name-player1-container');
    inputName1container.classList.remove('hidden');
    inputName1container.classList.add('name-input-flexbox');

    const confirmName1 = document.getElementById('change-name-player1-bt');
    confirmName1.addEventListener('click', () => getNameInput1(inputName1container));
}

// Após o clique do botão confirma. verifica se o nome não é vazio, nesse caso, não muda nada
//  se tiver algo, modifica o nome do player
function getNameInput1(containerName1) {
    const name1input = document.getElementById(`name-player-1`);
    
    if (name1input.value.trim() !== '') {
        player1.name = fixName(name1input.value.trim());
        const p1label = document.getElementById('player1-label');
        p1label.innerText = player1.name;
        Player1Name.innerText = `${player1.name}: `
        
    }
    containerName1.classList.remove('name-input-flexbox');
    containerName1.classList.add('hidden');
    changeName2();
}


// Repete os mesmos passos acima para o player2
function changeName2() {
    const inputName2container = document.getElementById('name-player2-container');
    inputName2container.classList.add('name-input-flexbox');
    inputName2container.classList.remove('hidden');
    const confirmName2 = document.getElementById('change-name-player2-bt');
    confirmName2.addEventListener('click', () => getNameInput2(inputName2container));
}

function getNameInput2(containerName2) {
    const name2input = document.getElementById('name-player-2');

    if(name2input.value.trim() !== '') {
        player2.name = fixName(name2input.value.trim());
        const p2label = document.getElementById('player2-label');
        p2label.innerText = player2.name;
        Player2Name.innerText = `${player2.name}: `
       
    }

    containerName2.classList.remove('name-input-flexbox');
    containerName2.classList.add('hidden');
    
}

const fixName = (name) => {
    name = name.split(' ');
    
    name = name.map( word => {
        word = word.split('');
        const firstLetter = word.shift();
        word.unshift(firstLetter.toUpperCase());
        return word.join('');
    });

    return name.join(' ');
}


// ============== EventListners ==============//
btnPlayAgain.addEventListener('click', () => {
    clearBoard();
});

function clearBoard() {
    divMain.innerHTML = '';
    let recoverDiv= document.createElement('div');
    recoverDiv.classList.add('hidden');
    recoverDiv.setAttribute('id', 'popUp');
    divMain.appendChild(recoverDiv);
    mapping = [];
    createBoard();
    jogadorDaVez = player1;
    infoPlayerONE.classList.add('itsturn');
    infoPlayerTWO.classList.remove('itsturn');
}

btnReset.addEventListener('click', () => {
    clearBoard();
    player1.score.innerText = 0;
    player2.score.innerText = 0;
});

btChangeName.addEventListener('click', changeName1);

playerONEPickColor.addEventListener('input', () => {
    cssRoot.style.setProperty('--player1', playerONEPickColor.value);
})

playerTWOPickColor.addEventListener('input', () => {
    cssRoot.style.setProperty('--player2', playerTWOPickColor.value);
})

divMain.addEventListener('click', e => play(e));


//========== Função que mantém as cores atualizadas ==========//
const getColors = () => {
    cssRoot.style.setProperty('--player1', playerONEPickColor.value);
    cssRoot.style.setProperty('--player2', playerTWOPickColor.value);
}
getColors();
